# Guidelines for designing cybersecurity serious games

This project contains methods and best practices for creating cybersecurity games and hands-on training exercises. The methods were created within the [master thesis](https://is.muni.cz/th/uovmy/) of Miriam Gáliková. The full text can be found in the [Wiki](https://gitlab.ics.muni.cz/muni-kypo-trainings/guidelines/-/wikis/home) section.

The applicability of the methods was tested during the creation of the [Junior hacker training](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/junior-hacker) game, which is freely available.

## License

The guidelines are licensed under [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/). 

## How to cite

Based on the proposed methods, we published the following [poster](https://dl.acm.org/doi/10.1145/3408877.3439568): 

M. Gáliková, V. Švábenský, J. Vykopal.\
*Toward Guidelines for Designing Cybersecurity Serious Games.*\
in Proceedings of the 52nd ACM Technical Symposium on Computer Science Education (SIGCSE), 2021.\
Available at: https://doi.org/10.1145/3408877.3439568. 

If you use or build upon the methods, please cite the poster using the following BibTeX entry:
```
@inproceedings{Galikova2021,
    author    = {G\'{a}likov\'{a}, Miriam and \v{S}v\'{a}bensk\'{y}, Valdemar and Vykopal, Jan},
    title     = {Toward Guidelines for Designing Cybersecurity Serious Games},
    year      = {2021},
    isbn      = {978-1-4503-8062-1},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    booktitle = {Proceedings of the 52nd ACM Technical Symposium on Computer Science Education (SIGCSE '21)},
    doi       = {10.1145/3408877.3439568},
    url       = {https://doi.org/10.1145/3408877.3439568},
    pages     = {1275},
    numpages  = {1}
}
```

## Credits

[Cybersecurity Laboratory](https://cybersec.fi.muni.cz)\
Faculty of Informatics\
Masaryk University

**Leading author:** Miriam Gáliková

**Contributors/Consultants:** Valdemar Švábenský, Jan Vykopal

Feel free to contact [Valdemar Švábenský](mailto:svabensky@ics.muni.cz?subject=MUNI%20KYPO%20Cybersecurity%20Games) if you have any feedback, requests, or questions.
